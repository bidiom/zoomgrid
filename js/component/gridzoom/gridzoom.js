define(["jquery", "underscore", "backbone"
],
function($, _, Backbone
){
    return Backbone.View.extend({
        className:"gridZoom",
        initialize:function(){
            this.picture = $("<img>", {
                "class":"picture"
            });

            this.grid = $("<canvas>", {
                "class":"grid"
            });

            this.$el.append(this.picture, this.grid);

            this.model.on("change:picture", this.onPictureChange, this);
            this.model.on("change:showGrid", this.onShowGridChange, this);
        },
        onPictureChange:function(model, pictureURL){
            this.picture.attr("src", pictureURL);
        },
        onShowGridChange:function(model, showGrid){
            if(showGrid){
                this.renderGrid();
                this.zoomKeypressHandlerEnabled(true);
            }else {
                this.hideGrid();
                this.zoomKeypressHandlerEnabled(false);
            }
        },
        renderGrid:function(){
            var rows = 3,
                cols = 3,
                grid = this.grid,
                ctx = grid[0].getContext("2d");

            console.log(grid.width(), grid.height());

            var cellWidth = grid.width() / cols,
                cellHeight = grid.height() / rows;

            this.model.set("cellIds", [
                "1","2","3","4","5","6","7","8","9"
            ]);

            console.log(cellWidth, cellHeight);

            ctx.moveTo(100,0);
            ctx.lineTo(100,grid.height());
            ctx.stroke();

            ctx.moveTo(200,0);
            ctx.lineTo(200,grid.height());
            ctx.stroke();

            ctx.moveTo(0,50);
            ctx.lineTo(grid.width(),50);
            ctx.stroke();

            ctx.moveTo(0,100);
            ctx.lineTo(grid.width(),100);
            ctx.stroke();

            ctx.font = "30px Arial";
            ctx.fillText("1",45,36);
            ctx.fillText("2",145,36);
            ctx.fillText("3",245,36);

            ctx.fillText("4",45,86);
            ctx.fillText("5",145,86);
            ctx.fillText("6",245,86);

            ctx.fillText("7",45,136);
            ctx.fillText("8",145,136);
            ctx.fillText("9",245,136);
        },
        hideGrid:function(){
            var grid = this.grid,
                ctx = grid[0].getContext("2d");

            ctx.clearRect(0,0,grid.width(), grid.height());
        },
        zoomKeypressHandlerEnabled:function(enabled){
            if(!enabled){
                $(document).off("keypress.cellId");
                return;
            }

            var cellIds = this.model.get("cellIds");
            $(document).on("keypress.cellId", _.bind(function(e){
                if(_.contains(cellIds, e.key)){
                    this.zoomCell(e.key);
                    $(document).off("keypress.cellId");
                }
            }, this));
        },
        zoomCell:function(cellId){
            this.hideGrid();

            var preZoomHeight = this.$el.height();
            this.$el.height(preZoomHeight);

            this.picture.width(this.picture.width()*3);

            this.enablePanning();
        },
        enablePanning:function(){
            var offset = {
                    x:0,
                    y:0
                },
                handlers = {
                "ArrowUp":function(){//up
                    offset.x += 100;
                    this.css("margin-top", offset.x);
                },
                "ArrowDown":function(){//down
                    offset.x -= 100;
                    this.css("margin-top", offset.x);
                },
                "ArrowRight":function(){
                    offset.y -= 100;
                    this.css("margin-left", offset.y);
                },
                "ArrowLeft":function(){
                    offset.y += 100;
                    this.css("margin-left", offset.y);
                }
            }

            $(document).on("keypress.panning", _.bind(function(e){
                if(handlers[e.key]){
                    handlers[e.key].apply(this.picture);
                }
                if("Escape" == e.key){
                    this.reset();
                    $(document).off("keypress.panning");
                }
            }, this));
        },
        reset:function(){
            this.picture.css("margin-top", "");
            this.picture.css("margin-left", "");
            this.picture.css("width", "");
        }
    });
});
define(["jquery",
    "controller/interaction",
    "component/gridzoom/gridzoommodel",
    "component/gridzoom/gridzoom",
    "data/pictures"
],
function($,
    InteractionController,
    GridZoomModel,
    GridZoom,
    pictures
){
    $(function(){
        var appNode = $("#app"),
            interactionController = new InteractionController(),
            gridZoomModel = new GridZoomModel(),
            gridZoom = new GridZoom({
                model:gridZoomModel
            }),
            pictureIndex = 0;

        appNode.append(gridZoom.el);

        setupEventConnections();
        gridZoomModel.set({"picture":pictures[pictureIndex]});
        interactionController.engage();

        function setupEventConnections(){
            interactionController.on("image.next", function(){
                pictureIndex = pictureIndex+1 < pictures.length ? pictureIndex+1 : 0;
                gridZoomModel.set({"picture":pictures[pictureIndex]});
            });
            interactionController.on("image.prev", function(){
                pictureIndex = pictureIndex-1 < 0 ? pictures.length-1 : pictureIndex-1;
                gridZoomModel.set({"picture":pictures[pictureIndex]});
            });

            interactionController.on("zoom.init", function(){
                gridZoomModel.set({"showGrid":true});
            });
            interactionController.on("ESC", function(){
                gridZoomModel.set({"showGrid":false});
            });
        }
    })
});
define(["jquery", "underscore", "backbone"
],
function($, _, Backbone
){
    return function(){
        var me = _.extend({}, Backbone.Events),
            keymap = {
                "0":"ESC",
                "106":"image.next",
                "107":"image.prev",
                "122":"zoom.init"
            };

        me.engage = function(){
            $(document).on("keypress", function(e){
                //console.log(e);
                if(keymap[e.which]){
                    me.trigger(keymap[e.which]);
                };
            });
        }

        return me;
    };
});